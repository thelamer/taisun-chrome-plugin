// Taisun.io Proxy set extension
// 2017 
// License MIT

// Helper for the current state of the proxy if none set assume false
function getState() {
  return localStorage['state'] || "false";
}

// Helper for the current setting for the proxy endpoint if not set assume false
function getproxy() {
  return localStorage['endpoint'] || "false";
}

// Set the proxy
function setProxy() {
  var state = getState();
  var endpoint = getproxy();
  // If not enabled on click enable it and set the foreground icon to on
  if (state != "false") {
    chrome.browserAction.setBadgeText({text: 'On'});
    chrome.browserAction.setBadgeBackgroundColor({color: [110, 210, 80, 180]});
    var host = endpoint.split(':')[0];
    var port = parseInt(endpoint.split(':')[1]);
    var config = {
      mode: "fixed_servers",
      rules: {
        singleProxy: {
          scheme: "https",
          host: host,
          port: port
        }
      }
    };
    chrome.proxy.settings.set({
      'value': config,
      'scope': 'regular'
    }, function() {});
  // Disable proxy if on
  } else {
    chrome.browserAction.setBadgeBackgroundColor({color: [130, 130, 130, 180]});
    chrome.browserAction.setBadgeText({text: 'Off'});
      var config = {mode: 'system'};
      chrome.proxy.settings.set({
        'value': config,
        'scope': 'regular'
      }, function() {});
      chrome.proxy.settings.clear({'scope': 'regular'});
    }
}

// When user flicks 
function clickListener() {
  var endpoint = getproxy();
  // Proxy setting is not configured send notification with link to options
  if (endpoint == "false"){
    var opt = {
      type: "basic",
      title: "Proxy Not Set",
      message: "Open options below and set your proxy settings to use this extension",
      iconUrl: "/img/icon48.png",
      buttons: [{title:"Options",iconUrl:"/img/gear.png"}]
    };
    chrome.notifications.create('test', opt);
  }
  // Setting is configured set flag and invoke proxy set
  else {
    var state = getState();
    if (state == "false") {
      state = "true";
    } else {
      state = "false";
    }
    localStorage['state'] = state;
    setProxy();
  }
}

// On startup of chrome purge proxy settings and reset icon
function startupListener() {
  chrome.browserAction.setIcon({path: "/img/icon32.png"});
  chrome.proxy.settings.clear({'scope': 'regular'});
}

// Send notification to user that the proxy does not have an endpoint configured
function sendoptions() {
  chrome.tabs.create({'url': "/html/options.html" } );
}

// Default actions
chrome.browserAction.onClicked.addListener(clickListener);
chrome.runtime.onStartup.addListener(startupListener);
chrome.notifications.onButtonClicked.addListener(sendoptions);
